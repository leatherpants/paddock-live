<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessionsResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sessions_results', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('gp_id')->index();
            $table->unsignedInteger('session_id')->index();
            $table->unsignedInteger('position');
            $table->unsignedInteger('driver_id')->index();
            $table->unsignedInteger('team_id')->index();
            $table->unsignedInteger('laptime');
            $table->unsignedInteger('laps');
        });

        Schema::table('sessions_results', function (Blueprint $table) {
            $table->foreign('gp_id')->references('id')->on('grand_prixs');
            $table->foreign('driver_id')->references('id')->on('drivers');
            $table->foreign('team_id')->references('id')->on('teams');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sessions_results');
    }
}
