<?php

use Faker\Generator as Faker;

$factory->define(\App\live\Teams\Models\Teams::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});
