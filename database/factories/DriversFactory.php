<?php

use Faker\Generator as Faker;

$factory->define(\App\live\Drivers\Models\Drivers::class, function (Faker $faker) {
    return [
        'number' => $faker->randomDigit,
        'name' => $faker->name,
    ];
});
