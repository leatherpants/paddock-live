<?php

use Faker\Generator as Faker;

$factory->define(\App\live\GrandPrixs\Models\GrandPrixs::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});
