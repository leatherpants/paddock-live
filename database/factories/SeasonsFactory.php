<?php

use Faker\Generator as Faker;

$factory->define(\App\live\Seasons\Models\Seasons::class, function (Faker $faker) {
    return [
        'season' => $faker->year,
    ];
});
