<?php

use Faker\Generator as Faker;

$factory->define(\App\live\Sessions\Models\SessionsResults::class, function (Faker $faker) {
    return [
        'gp_id' => function () {
            return factory(\App\live\GrandPrixs\Models\GrandPrixs::class)->create()->id;
        },
        'session_id' => $faker->randomDigit,
        'position' => $faker->randomDigit,
        'driver_id' => function () {
            return factory(\App\live\Drivers\Models\Drivers::class)->create()->id;
        },
        'team_id' => function () {
            return factory(\App\live\Teams\Models\Teams::class)->create()->id;
        },
        'laptime' => $faker->randomNumber,
        'laps' => $faker->randomDigit,
    ];
});
