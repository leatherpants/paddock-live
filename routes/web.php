<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Frontend'], function () {
    Route::get('live', ['uses' => 'LiveController@index', 'as' => 'live']);
});
