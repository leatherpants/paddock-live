<?php

namespace Tests\Feature;

use Tests\TestCase;

class LiveTest extends TestCase
{
    /** @test */
    public function test_show_the_live_page()
    {
        $response = $this->get(route('live'));
        $response->assertSuccessful();
    }
}
