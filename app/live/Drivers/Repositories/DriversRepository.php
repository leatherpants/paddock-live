<?php

namespace App\live\Drivers\Repositories;

use App\live\Drivers\Models\Drivers;

class DriversRepository
{
    /**
     * @var Drivers
     */
    private $drivers;

    /**
     * DriversRepository constructor.
     * @param Drivers $drivers
     */
    public function __construct(Drivers $drivers)
    {
        $this->drivers = $drivers;
    }
}
