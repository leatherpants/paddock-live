<?php

namespace App\live\Drivers\Models;

use Illuminate\Database\Eloquent\Model;

class Drivers extends Model
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'number',
        'name',
    ];

    /**
     * Indicates if the model should be timestamped.
     * @var bool
     */
    public $timestamps = false;
}
