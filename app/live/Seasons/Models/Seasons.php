<?php

namespace App\live\Seasons\Models;

use Illuminate\Database\Eloquent\Model;

class Seasons extends Model
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'season',
    ];

    /**
     * Indicates if the model should be timestamped.
     * @var bool
     */
    public $timestamps = false;
}
