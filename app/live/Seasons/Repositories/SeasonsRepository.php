<?php

namespace App\live\Seasons\Repositories;

use App\live\Seasons\Models\Seasons;

class SeasonsRepository
{
    /**
     * @var Seasons
     */
    private $seasons;

    /**
     * SeasonsRepository constructor.
     * @param Seasons $seasons
     */
    public function __construct(Seasons $seasons)
    {
        $this->seasons = $seasons;
    }
}
