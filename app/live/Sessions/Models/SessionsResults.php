<?php

namespace App\live\Sessions\Models;

use Illuminate\Database\Eloquent\Model;

class SessionsResults extends Model
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'gp_id',
        'session_id',
        'position',
        'driver_id',
        'team_id',
        'laptime',
        'laps',
    ];

    /**
     * Indicates if the model should be timestamped.
     * @var bool
     */
    public $timestamps = false;
}
