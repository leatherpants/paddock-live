<?php

namespace App\live\Sessions\Repositories;

use App\live\Sessions\Models\SessionsResults;

class SessionsResultsRepository
{
    /**
     * @var SessionsResults
     */
    private $sessionsResults;

    /**
     * SessionsResultsRepository constructor.
     * @param SessionsResults $sessionsResults
     */
    public function __construct(SessionsResults $sessionsResults)
    {
        $this->sessionsResults = $sessionsResults;
    }
}
