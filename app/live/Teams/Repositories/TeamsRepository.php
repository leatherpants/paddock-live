<?php

namespace App\live\Teams\Repositories;

use App\live\Teams\Models\Teams;

class TeamsRepository
{
    /**
     * @var Teams
     */
    private $teams;

    /**
     * TeamsRepository constructor.
     * @param Teams $teams
     */
    public function __construct(Teams $teams)
    {
        $this->teams = $teams;
    }
}
