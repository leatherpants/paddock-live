<?php

namespace App\live\Teams\Models;

use Illuminate\Database\Eloquent\Model;

class Teams extends Model
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Indicates if the model should be timestamped.
     * @var bool
     */
    public $timestamps = false;
}
