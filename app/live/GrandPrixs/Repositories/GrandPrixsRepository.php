<?php

namespace App\live\GrandPrixs\Repositories;

use App\live\GrandPrixs\Models\GrandPrixs;

class GrandPrixsRepository
{
    /**
     * @var GrandPrixs
     */
    private $grandPrix;

    /**
     * GrandPrixsRepository constructor.
     * @param GrandPrixs $grandPrix
     */
    public function __construct(GrandPrixs $grandPrix)
    {
        $this->grandPrix = $grandPrix;
    }
}
